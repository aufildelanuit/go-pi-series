package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"time"
)

// Equal tells whether a and b contain the same elements.
// A nil argument is equivalent to an empty slice.
func equal(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func calculationStep(n int64, x float64, scale float64) float64 {
	return (x + math.Pow(-1, float64(n))*(nthplusdigit(float64(4/((2*float64(n))+1)), scale)))
}

func compensation(n int64, x float64, prevxup *float64, prevxdown *float64, up []int, down []int, scale *float64) float64 {
	var upstable, downstable bool
	upstable = false
	downstable = false
	var comp float64
	if (n % 2) == 0 {
		*prevxup = x
		up[4] = up[3]
		up[3] = up[2]
		up[2] = up[1]
		up[1] = up[0]
		up[0] = nthdigit(*prevxup, *scale)
	} else {
		*prevxdown = x
		down[4] = down[3]
		down[3] = down[2]
		down[2] = down[1]
		down[1] = down[0]
		down[0] = nthdigit(*prevxdown, *scale)
	}
	//println("  up : ", up[0], up[1], up[2], up[3], up[4])
	//println("  down : ", down[0], down[1], down[2], down[3], down[4])
	if (up[0] == up[1]) && (up[1] == up[2]) {
		if (up[2] == up[3]) && (up[3] == up[4]) {
			upstable = true
		}
	}
	if (down[0] == down[1]) && (down[1] == down[2]) {
		if (down[2] == down[3]) && (down[3] == down[4]) {
			downstable = true
		}
	}
	if upstable && downstable {
		if up[0] != down[0] {
			comp = ((nthplusdigit(*prevxup, *scale) - nthplusdigit(*prevxdown, *scale)) / 2)
		}
		*scale *= 0.1
		up[4] = 0
		up[3] = 1
		up[2] = 0
		up[1] = 1
		up[0] = 0
		down[4] = 1
		down[3] = 0
		down[2] = 1
		down[1] = 0
		down[0] = 1
	}
	if (n % 2) == 0 {
		return (-comp)
	}
	return (comp)
}

func doCalculation(result chan<- float64, signal chan bool, compensated bool) {
	var n int64
	var x, prevxup, prevxdown, scale, comp float64
	scale = 1
	up := make([]int, 5)
	down := make([]int, 5)
	for {
		x = calculationStep(n, x, scale)
		if compensated && (n > 1000) {
			comp = compensation(n, x, &prevxup, &prevxdown, up, down, &scale)
			if comp != 0 {
				print("step ", n, " x uncompensated: ", x)
				x += comp
				println("  compensation = ", comp, " scale = ", scale, " result = ", x)
			}
		}
		select {
		case result <- x:
			// if the result can be sent to the channel, nothing more to do for now
		case <-signal:
			fmt.Println("## Signal received, calculation stopped")
			result <- x
			close(result)
			signal <- true
			return
		default:
			//something to do in the default case (nothing here?)
		}
		if n > 0 {
			//time.Sleep(300 * time.Millisecond)
		}
		n++
	}
}

func nthdigit(x float64, n float64) int {
	return (int(x/n) % 10)
}

func nthplusdigit(x float64, n float64) float64 {
	var i float64
	i = 1
	if x < n {
		return x
	}
	for i > n {
		x -= float64(nthdigit(x, i)) * i
		i /= 10
	}
	return x
}

func waitForInput(signal chan<- bool) {
	reader := bufio.NewReader(os.Stdin)
	var input byte
	for input == 0 {
		input, _ = reader.ReadByte()
	}
	signal <- true
	signal <- true
	fmt.Println("## Keypressed, signal sent")
	return
}

func main() {
	result := make(chan float64, 1)
	signal := make(chan bool, 3)
	compensation := true
	var res float64
	go doCalculation(result, signal, compensation)
	go waitForInput(signal)
	//go autoCompensation(result, signal, compResult)
	fmt.Println("Pi calculation, press <Enter> to stop")
	for {
		select {
		case <-signal:
			fmt.Println("## Main loop, signal received")
			time.Sleep(300 * time.Millisecond)
			fmt.Println("## Main loop, closing channels")
			close(signal)
			fmt.Println("## quit")
			return
		case res = <-result:
			fmt.Print("\r  pi ~  ", res, "  ", "   \t\t\t\r")
		}
		time.Sleep(300 * time.Millisecond)
	}
}
